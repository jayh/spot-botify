from flask import Flask, request
import pycountry

from spotify import Spotify

spotify = Spotify()


def handle_categories(incoming_request):
    entity_number = incoming_request.json['queryResult']['parameters']['number']
    number_of_categories = '20'
    text = 'Here are the top ' + number_of_categories + ' categories'
    if entity_number != '':
        number_of_categories = str(int(entity_number))
        text = 'Here are the top ' + number_of_categories + ' categories'
    elif 'category' in incoming_request.json['queryResult']['queryText']:
        number_of_categories = '1'
        text = "The top cateogry is:"

    res = spotify.get_categories(number_of_categories)

    response = {
        "fulfillmentMessages": [
            {
                "text": {
                    "text": [text]
                },
                "platform": "Slack"
            },
            {
                "text": {
                    "text": [res]
                },
                "platform": "Slack"
            }
        ]
    }
    return response


def handle_artist_search(incoming_request):
    artist_name_entity = incoming_request.json['queryResult']['parameters']['music-artist']
    res = spotify.find_artist(artist_name_entity)

    if res['id'] is None:
        text = f'No artist named { res["name"] } found'
    else:
        text = f'Found artist: { res["name"] }'

    response = {
        "fulfillmentMessages": [
            {
                "text": {
                    "text": [text]
                },
                "platform": "Slack"
            }
        ]
    }
    return response


def handle_get_albums(incoming_request):
    output_contexts = incoming_request.json['queryResult']['outputContexts']

    artist_name_entity = ''
    for context in output_contexts:
        if 'music-artist' in context['parameters']:
            artist_name_entity = context['parameters']['music-artist']

    albums = spotify.get_albums(artist_name_entity)

    follow_up_text = ''
    if len(albums) == 0:
        text = f'No albums found for {artist_name_entity}'
    else:
        text = f'Here are some albums for {artist_name_entity}'
        for i in albums:
            follow_up_text += (i + ', ')

    response = {
        "fulfillmentMessages": [
            {
                "text": {
                    "text": [text]
                },
                "platform": "Slack"
            }
        ]
    }

    if follow_up_text != '':
        response['fulfillmentMessages'].append({
            "text": {
                "text": [follow_up_text]
            },
            "platform": "Slack"
        })

    return response


def hanlde_top_tracks(incoming_request):
    output_contexts = incoming_request.json['queryResult']['outputContexts']

    artist_name_entity = ''
    for context in output_contexts:
        if 'music-artist' in context['parameters']:
            artist_name_entity = context['parameters']['music-artist']

    country = request.json['queryResult']['parameters']['geo-country']

    if country is '':
        return {
            "fulfillmentMessages": [
                {
                    "text": {
                        "text": ["For which country?"]
                    },
                    "platform": "Slack"
                }
            ]
        }

    country_code = pycountry.countries.get(name=country).alpha_2

    tracks = spotify.get_top_tracks(artist_name_entity, country_code)

    follow_up_text = ''
    if len(tracks) == 0:
        text = f'No tracks found for artist {artist_name_entity} in {country}'
    else:
        text = f'Here are the top tracks for {artist_name_entity} in {country}'
        for t in tracks:
            follow_up_text += (t['name'] + ', ')

    response = {
        "fulfillmentMessages": [
            {
                "text": {
                    "text": [text]
                },
                "platform": "Slack"
            }
        ]
    }

    if follow_up_text != '':
        response['fulfillmentMessages'].append({
            "text": {
                "text": [follow_up_text]
            },
            "platform": "Slack"
        })

    return response


app = Flask(__name__)
@app.route('/', methods=['POST'])
def resolve_api():
    print(request.json['queryResult'])
    intent_name = request.json['queryResult']['intent']['displayName']

    if intent_name == 'categories':
        return handle_categories(request)
    elif intent_name == 'find-artists':
        return handle_artist_search(request)
    elif intent_name == 'get-albums':
        return handle_get_albums(request)
    elif intent_name == 'top-tracks':
        return hanlde_top_tracks(request)


app.run(port=3000)
