import base64
import os
import requests

CLIENT_ID = os.environ['SPOTIFY_CLIENT_ID']
CLIENT_SECRET = os.environ['SPOTIFY_CLIENT_SECRET']

top_categories = {}
artist_details = {}


class Spotify:
    def __init__(self):
        self.token = self.authorization()

    def authorization(self):
        url = 'https://accounts.spotify.com/api/token'
        token = CLIENT_ID + ':' + CLIENT_SECRET
        encoded_token = base64.b64encode(token.encode('utf-8'))
        headers = {
          'Authorization': 'Basic ' + str(encoded_token, 'utf-8')
        }
        body = {
          'grant_type': 'client_credentials'
        }

        res = requests.post(url, headers=headers, data=body)
        token = res.json()['access_token']
        return token

    def get_categories(self, number_of_categories):
        url = f'https://api.spotify.com/v1/browse/categories?limit={number_of_categories}'

        headers = {
          'Authorization': 'Bearer ' + self.token
        }
        res = requests.get(url, headers=headers)
        categories_response = res.json()
        items = categories_response['categories']['items']

        top_lists = ''
        for i in items:
            top_categories[i['name']] = i['id']
            if top_lists != '':
                top_lists += ','
                top_lists = f'{top_lists} {i["name"]}'

        return top_lists

    def get_playlists(self, category_id):
        url = f'https://api.spotify.com/v1/browse/categories/{category_id}/playlists'

        headers = {
            'Authorization': 'Bearer ' + self.token
        }
        res = requests.get(url, headers=headers)
        playlists_response = res.json()
        items = playlists_response['playlists']['items']

        top_playlists = ''
        for i in items:
            if top_playlists != '':
                top_playlists += ','
            top_playlists = f'{top_playlists} {i["name"]}'

        return top_playlists

    def find_artist(self, artist_name):
        name = artist_name.lower()
        name.replace(' ', '%20')

        url = 'https://api.spotify.com/v1/search'
        headers = {
          'Authorization': 'Bearer ' + self.token
        }

        params = {
          'q': name,
          'type': 'artist'
        }

        res = requests.get(url, headers=headers, params=params)

        if res.status_code == 200:
            artist_found = res.json()['artists']['items'][0]
            artist_res = {
                'id': artist_found['id'],
                'name': artist_found['name']
            }
            artist_details[artist_found['name']] = artist_found['id']
        else:
            artist_res = {
                'id': None,
                'name': artist_name
            }

        return artist_res

    def get_albums(self, artist_name):
        if artist_name not in artist_details:
            return []

        artist_id = artist_details[artist_name]

        url = f'https://api.spotify.com/v1/artists/{artist_id}/albums?limit=5'
        headers = {
            'Authorization': 'Bearer ' + self.token
        }

        res = requests.get(url, headers=headers)

        albums = []
        if res.status_code == 200:
            items = res.json()['items']
            for i in items:
                albums.append(i['name'])

        return albums

    def get_top_tracks(self, artist_name, country_code):
        if artist_name not in artist_details:
            return []

        artist_id = artist_details[artist_name]

        url = f'https://api.spotify.com/v1/artists/{artist_id}/top-tracks?country={country_code}'
        headers = {
            'Authorization': 'Bearer ' + self.token
        }

        res = requests.get(url, headers=headers)

        tracks = []
        if res.status_code == 200:
            tracks = res.json()['tracks']

        return tracks
